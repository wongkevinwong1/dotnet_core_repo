﻿using CakeShopMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CakeShopMVC.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Pie>  PiesOfTheWeek { get; set; }
    }
}
